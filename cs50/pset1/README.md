# CS50: Problem Set 1

## Prerequisites

* Install `svn`
```
$ sudo apt-get install subversion
```
* Install `clang`
```
$ sudo apt-get install clang
```
* Install `make`
```
$ sudo apt-get install make
```
* Install [libcs50](https://github.com/cs50/libcs50#installation)
```
$ sudo apt-add-repository ppa:cs50/ppa
$ sudo apt-get update
$ sudo apt-get install libcs50
```

## Usage

#### Download
```
$ svn checkout https://github.com/rotyflo/study/trunk/cs50/pset1
$ cd pset1
```
#### Compile
```
$ make all
```
#### Execute
```
$ ./hello
$ ./water
$ ./mario
$ ./credit
```
