// Determines whether a provided credit card number is valid according to Luhn’s algorithm.

#include <stdio.h>
#include <cs50.h>
#include <string.h>

void verify();
int numlen();

int main(void)
{
    printf("What is your credit card number?\n");
    long long card_num = get_long_long();

    verify(card_num);
}

void verify(long long n)
{
    int sum = 0;
    int position = 0;
    int first_digit = 0;
    int first_two_digits = 0;
    int length = numlen(n);

    for (int i = length; i > 0; i--)
    {
        if (i == 2)
        {
            first_two_digits = n;
        }
        else if (i == 1)
        {
            first_digit = n;
        }

        position--;

        int digit = n % 10;

        if (position % 2 == 0)
        {
            digit *= 2;
            sum += digit % 10;
            digit /= 10;
            sum += digit % 10;
        }
        else
        {
            sum += digit;
        }

        n /= 10;
    }

    // If the last digit of 'sum' is 0
    if (sum % 10 == 0)
    {
        // If no number was entered
        if (first_two_digits == 0)
        {
            printf("INVALID\n");
        }
        else if (first_digit == 4)
        {
            if (length == 13 || length == 16)
            {
                printf("VISA\n");
            }
            else
            {
                printf("INVALID\n");
            }
        }
        else
        {
            switch (first_two_digits)
            {
                case 34:
                case 37:
                    if (length == 15)
                    {
                        printf("AMEX\n");
                    }
                    else
                    {
                        printf("INVALID\n");
                    }
                    break;

                case 51:
                case 52:
                case 53:
                case 54:
                case 55:
                    if (length == 16)
                    {
                        printf("MASTERCARD\n");
                    }
                    else
                    {
                        printf("INVALID\n");
                    }
                    break;
            }
        }
    }
    else
    {
        printf("INVALID\n");
    }
}

int numlen(long long n)
{
    char *s;
    sprintf(s, "%lld", n);
    return strlen(s);
}